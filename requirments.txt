beautifulsoup4==4.7.1
pandas==0.24.2
scikit-learn==0.21.2
PyYAML==5.1
# clone and install fastText and fairseq
# -e git://github.com/pytorch/fairseq.git@881381cfc7cf5006712181138ff5b0811052cc57#egg=fairseq
#-e git://github.com/facebookresearch/fastText@6dd2e11b5fe82854c4529d2a58d699b2cb182b1b#egg=fastText
torch==1.1.0

