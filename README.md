# Convolution Seq2Seq Model for Multi Label Multi Class Hierarchical Classification - Germeval Shared Task 1

This repo contains our code for the Germeval Shared Task 1. Our Team won the first rank in subtask (a) and second rank in subtask (b). Our preprocessed data, trained model and training logs are made available [here](https://drive.google.com/open?id=1T6BBhghxfGT6nfdF-7Sb7LIyVxWQQwW1)

# How to run?

1. Install Dependencies given in requirements.txt. Please note you need to clone to gitrepo of fairseq and fastText locally and then install them manually. Use pip to directly install a specific commit doesn't work
2. `python main.py <DATAFOLDER> <GPUID>`