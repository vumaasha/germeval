import csv
import os
from tqdm import tqdm
import numpy as np
import random

from fastText import train_unsupervised
import logging

logger = logging.getLogger(__name__)


def learn_embeddings(train, val, test, base_path, source_dict_file, target_dict_file, label_shuffling_rounds=0):
    desc = train.input_text.append(val.input_text).append(test.input_text)
    desc_file = os.path.join(base_path, 'des.txt')
    desc.to_csv(desc_file, index=False, header=False, quoting=csv.QUOTE_NONE, sep='#')
    logger.info("desc export for fastText completed.")

    tgt = train.labels.append(val.labels)
    tgt_file = os.path.join(base_path, 'cat.txt')

    def shuffle_label_ordering(label_list):
        labels = label_list.split('\t')
        random.shuffle(labels)
        labels = '\t'.join(labels)
        return labels

    # the ordering of labels should not matter, so to learn better embeddings we augment the data with
    # several examples with random label ordering

    for i in range(label_shuffling_rounds):
        tgt = tgt.append(tgt.apply(shuffle_label_ordering))

    tgt.to_csv(tgt_file, index=False, header=False, quoting=csv.QUOTE_NONE, sep='#')
    logger.info("category export for fastText completed.")

    logger.info("learning desc embeddings.")
    # desc_model = train_unsupervised(input=desc_file, model='skipgram', verbose=2)
    desc_model = train_unsupervised(input=desc_file, model='skipgram', verbose=2, minCount=2, epoch=20, dim=100)
    desc_model.save_model(os.path.join(base_path, 'desc.bin'))
    logger.info("learning desc embeddings completed.")

    logger.info("learning category embeddings.")
    # tgt_model = train_unsupervised(input=tgt_9file, model='skipgram', verbose=2)
    tgt_model = train_unsupervised(input=tgt_file, model='skipgram', verbose=2, minCount=1, epoch=100, dim=100)
    tgt_model.save_model(os.path.join(base_path, 'tgt.bin'))
    logger.info("learning category embeddings completed.")

    logger.info("exporting desc embeddings.")
    export_embedding(desc_model, source_dict_file, os.path.join(base_path, 'desc_embeddings.txt'))
    logger.info("exporting desc embeddings completed.")

    logger.info("exporting cat embeddings.")
    export_embedding(tgt_model, target_dict_file, os.path.join(base_path, 'cat_embeddings.txt'))
    logger.info("exporting cat embeddings completed.")


def export_embedding(model, dict_file, export_path):
    def parse_vocab(line):
        pieces = line.split()
        return (pieces[0], pieces[1])

    with open(dict_file) as df:
        vocab_items = (parse_vocab(line) for line in df)
        vocab = dict(vocab_items)
    emb_dim_size = model.get_dimension()
    vocab_size = len(vocab)
    with open(export_path, 'w') as emb_export:
        emb_export.write('{} {}\n'.format(vocab_size, emb_dim_size))
        for token in tqdm(vocab):
            embedding = model.get_word_vector(token)
            norm = np.linalg.norm(embedding)
            embedding /= norm
            embedding_str = ' '.join(['{:.8f}'.format(i) for i in embedding.tolist()])
            embedding_str = '{} {}\n'.format(token, embedding_str)
            emb_export.write(embedding_str)
