import re


class Category(object):
    def __init__(self, name, parent=None):
        self.parent = parent
        self.name = name
        self.child = None
        self.path = None

    def is_leaf(self):
        return self.child is None

    def is_root(self):
        return self.name.lower() == 'root'

    def is_intermediate(self):
        return not (self.is_leaf() or self.is_root())

    def get_hierarchical_path(self):
        if self.path is None:
            path = [self.name]
            parent = self.parent
            while (not parent.is_root()):
                path.append(parent.name)
                parent = parent.parent
            path.reverse()
            self.path = path
        return '\t'.join(self.path)

    def get_top_level(self):
        if self.path is None:
            self.get_hierarchical_path()
        return self.path[0]


class Hierarchy(object):
    def __init__(self, hierarchy_text):
        root = Category("Root")
        self.categories = {}
        self.categories[root.name] = root
        self.top_levels = set()
        with open(hierarchy_text) as hierarchy:
            for line in hierarchy:
                line = line.replace(' ', '~')
                parent, child = line.strip().split('\t')
                parent_category = self.categories.get(parent, Category(parent, root))
                self.categories[parent] = parent_category

                child_category = self.categories.get(child, Category(child))
                child_category.parent = parent_category
                self.categories[child] = child_category
        for category in self.categories:
            if category != 'Root' and self.categories[category].parent.name.lower() == 'root':
                self.top_levels.add(self.categories[category].name)

    def get_path(self, category_name):
        cat = self.categories[category_name]
        return cat.get_hierarchical_path()

    def reverse_lookup_all_levels(self, categories):
        cats = re.split(r'\s', categories)
        full_path_string = '\t'.join([self.get_path(cat) for cat in cats])
        all_levels = set(re.split(r'\s', full_path_string))
        return '\t'.join(all_levels)

    def reverse_lookup_top_levels(self, categories):
        all_levels = re.split(r'\s', self.reverse_lookup_all_levels(categories))
        top_levels = [level for level in all_levels if level in self.top_levels]
        return '\t'.join(top_levels)


