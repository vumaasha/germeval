from bs4 import BeautifulSoup
import pandas as pd
import re
from math import ceil
import logging
from collections import defaultdict
from random import choices
from math import ceil

logger = logging.getLogger(__name__)

def extract_data_from_book(book):
    record = {}
    # let's remove all the # in the body and title, so that we can use it as delimiter later
    record['title'] = book.title.text.replace('#', '')
    record['body'] = book.body.text.replace('#', '')
    categories = []
    if book.categories:
        for category in book.categories.find_all('category'):
            for topic in category.find_all('topic'):
                if 'label' in topic.attrs and topic['label'] == 'True':
                    # we are extracting the specific label only
                    # We will use the hierarchy object to look up for the ancestors later
                    categories.append(topic.text.strip())
    record['categories'] = categories
    authors = [author.strip() for author in book.authors.text.split(',')]
    record['authors'] = authors
    record['year'] = book.published.text.split('-')[0]
    input_text = ''
    author_text = ' '.join(
        ['@AUTHOR ' + author.replace(' ', '~') + ' AUTHOR@ ' for author in authors if author]) if authors else ''
    title_text = '@TITLE ' + record['title'] + ' TITLE@ '
    year_text = '@YEAR ' + record['year'] + ' YEAR@ ' if record['year'] else ''

    # we will introduce space between punctuations, special chars and words so that these characters are  considered
    # as tokens when split by space by fairseq later
    replace_pattern = re.compile(r'(\W+)')
    input_text = replace_pattern.sub(r' \1 ', title_text + record['body'])
    input_text = author_text + year_text + input_text

    if book.isbn:
        isbn = book.isbn.text
        record['isbn'] = isbn
        record['ISBN_GRP'] = isbn[4:6]
        record['ISBN_PUBLISHER'] = isbn[6:10]
        isbn_grp_text = '@ISBN_GRP {} ISBN_GRP@'.format(record['ISBN_GRP'])
        isbn_pub_text = '@ISBN_PUB {} ISBN_PUB@'.format(record['ISBN_PUBLISHER'])
        input_text = '{} {} {}'.format(input_text,isbn_grp_text,isbn_pub_text)
    record['input_text'] = ' '.join(input_text.split()[:670])
    if categories:
        record['labels'] = '\t'.join([cat.replace(' ', '~') for cat in categories])
    return record


def preprocess(input_file):
    with open(input_file, 'r') as f:
        html_doc = f.read()
        soup = BeautifulSoup(html_doc, 'html.parser')
        all_books = soup.find_all("book")
        book_records = map(extract_data_from_book, all_books)
        books_df = pd.DataFrame.from_records(book_records, index='isbn')
        books_df['category_count'] = books_df.categories.apply(lambda x: len(x))
    return books_df


def over_sample_by_category(books_df, hierarchy, boost_categories, pct=0.15, boosting_rounds=1):
    samples_to_generate = books_df.shape[0] * pct
    category_map = defaultdict(list)
    category_upsample_count = {}

    for i in books_df.itertuples():
        idx = i.Index
        lables = i.labels.split('\t')
        for l in lables:
            category_map[l].append(idx)
    category_count = {k: len(v) for k, v in category_map.items()}
    category_count_ = pd.Series(category_count)
    mean_size = int(category_count_.mean())
    minority_categories = category_count_[category_count_ < category_count_.mean()].index.unique()
    mean_increment = ceil(samples_to_generate / len(minority_categories))

    samples = []
    actual_samples_generated = 0
    for cat in minority_categories:
        mean_diff = mean_size - category_count[cat]
        num_samples_to_add = min(mean_diff, mean_increment)
        actual_samples_generated += num_samples_to_add
        category_upsample_count[cat] = num_samples_to_add
        indices = choices(category_map[cat], k=num_samples_to_add)
        cat_samples = books_df.loc[indices].copy()
        cat_samples['labels'] = cat
        samples.append(cat_samples)

        for round in range(boosting_rounds):
            cat_ = hierarchy.categories[cat]
            if cat_.get_top_level() in boost_categories:
                samples_to_add = min(mean_increment, category_count[cat])
                indices = choices(category_map[cat], k=samples_to_add)
                cat_samples = books_df.loc[indices].copy()
                cat_samples['labels'] = cat
                samples.append(cat_samples)

    upsamples_df = pd.concat(samples)
    return books_df.append(upsamples_df), actual_samples_generated


def power_set_over_sample(books_df, pct=0.1):
    # let's increase by dataset by pct percentage
    logger.info('Increasing the Dataset size by {:.2f}%'.format(pct * 100))
    samples_to_generate = books_df.shape[0] * pct
    label_powerset_freq = books_df.labels.value_counts()

    mean_size = int(label_powerset_freq.mean())
    logger.info('PowerSet label freq mean:{}'.format(mean_size))
    minority_lps = label_powerset_freq[label_powerset_freq < mean_size].sort_values(ascending=False).to_frame()

    logger.info('Total PowerSets:{}, Majority PowerSets:{}, Minority PowerSets:{}'.format(label_powerset_freq.shape[0], \
                                                                                          label_powerset_freq.shape[0] -
                                                                                          minority_lps.shape[0], \
                                                                                          minority_lps.shape[0]))

    mean_increment = ceil(samples_to_generate / minority_lps.shape[0])
    logger.info('Adding {:d} samples to each minority labelset on average'.format(mean_increment))

    minority_lps['upsample_count'] = minority_lps['labels'].apply(lambda x: min(mean_size - x, mean_increment))
    actual_samples_generated = minority_lps.upsample_count.sum()

    minority_lp_samples = []
    for lp in minority_lps.itertuples():
        samples = books_df[books_df.labels == lp.Index].sample(lp.upsample_count, replace=True)
        minority_lp_samples.append(samples)
    upsamples_df = pd.concat(minority_lp_samples)

    if samples_to_generate > upsamples_df.shape[0]:
        remainder = samples_to_generate - upsamples_df.shape[0]
        upsamples_lbl_freq = upsamples_df.labels.value_counts()
        min_lbl_mask = books_df.labels.isin(upsamples_lbl_freq[upsamples_lbl_freq < mean_size].index)
        remaining_samples_df = books_df[min_lbl_mask].sample(remainder)
        upsamples_df = upsamples_df.append(remaining_samples_df)

    new_train_upsampled_df = books_df.append(upsamples_df)
    return new_train_upsampled_df, actual_samples_generated
