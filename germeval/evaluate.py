import os
import re
from collections import defaultdict
import logging
import pandas as pd
from germeval.evaluation.evaluate import get_scores

logger = logging.getLogger(__name__)


def parse_predictions(fairseq_folder, hierarchy, actual_df):
    records = defaultdict(dict)
    predictions_file = os.path.join(fairseq_folder, 'results/predicted.txt')
    with open(predictions_file) as results:
        pattern = r'[STH]-(\d+)\t(.*)'
        for line in results:
            match = re.match(pattern, line)
            if match:
                rec_id = int(match.group(1))
                if line[0] == 'S':
                    records[rec_id]['source'] = match.group(2)
                elif line[0] == 'T':
                    records[rec_id]['actual'] = match.group(2)
                elif line[0] == 'H':
                    records[rec_id]['predicted'] = match.group(2).split('\t')[1]

    sorted_records = [records[k] for k in sorted(records.keys())]
    results_df = pd.DataFrame.from_records(sorted_records)
    
    if results_df['actual'].nunique() == 1:
        results_df['actual_top_levels'] = '?'
        results_df['actual_all_levels'] = '?'
    else:
        results_df['actual_top_levels'] = results_df['actual'].apply(hierarchy.reverse_lookup_top_levels)
        results_df['actual_all_levels'] = results_df['actual'].apply(hierarchy.reverse_lookup_all_levels)

    results_df['predicted_top_levels'] = results_df['predicted'].apply(hierarchy.reverse_lookup_top_levels)
    results_df['predicted_all_levels'] = results_df['predicted'].apply(hierarchy.reverse_lookup_all_levels)
    results_df.index = actual_df.index
    return results_df


def evaluate_model(fairseq_folder, results_df):
    fairseq_folder = os.path.join(fairseq_folder, 'results')

    logger.info('Writing predictions into file')
    answer_file = os.path.join(fairseq_folder, 'answer.txt')
    with open(answer_file, 'w') as gold:
        gold.write('subtask_a\n')
        for r in results_df.itertuples():
            gold.write(r.Index + '\t' + r.predicted_top_levels.replace('~', ' ') + '\n')
        gold.write('subtask_b\n')
        for r in results_df.itertuples():
            gold.write(r.Index + '\t' + r.predicted_all_levels.replace('~', ' ') + '\n')
    logger.info('Writing predictions into file completed')

    if results_df['actual'].nunique() > 1:
        logger.info('Writing gold labels into file')
        gold_file = os.path.join(fairseq_folder, 'gold.txt')
        with open(gold_file, 'w') as gold:
            gold.write('subtask_a\n')
            for r in results_df.itertuples():
                gold.write(r.Index + '\t' + r.actual_top_levels.replace('~', ' ') + '\n')
            gold.write('subtask_b\n')
            for r in results_df.itertuples():
                gold.write(r.Index + '\t' + r.actual_all_levels.replace('~', ' ') + '\n')
        logger.info('Writing gold labels into file completed')


        get_scores(fairseq_folder,fairseq_folder)

        with open('{}/scores.txt'.format(fairseq_folder)) as st:
            logger.info(st.read())

        with open('{}/scores.html'.format(fairseq_folder)) as st:
            logger.info(st.read())

