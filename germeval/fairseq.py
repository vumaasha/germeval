import logging
import os
from sklearn.model_selection import train_test_split

from germeval.preprocessing import over_sample_by_category
from .preprocessing import power_set_over_sample
import subprocess
from collections import Counter
from germeval.fastText import learn_embeddings
import csv
from fairseq.tokenizer import tokenize_line

logger = logging.getLogger(__name__)


def create_dict(train, val, test, fairseq_folder, lang):
    col = 'input_text' if lang == 'des' else 'labels'
    input_text = train[col].append(val[col]).append(test[col]).values
    source_dict = Counter([token for item in input_text for token in tokenize_line(item)])
    source_vocab = source_dict.most_common()
    logger.debug("{} tokens in {} dict".format(len(source_vocab), lang))
    dict_file = os.path.join(fairseq_folder, 'dict.{}.txt'.format(lang))
    with open(dict_file, 'w') as sdf:
        for vocab_item in source_vocab:
            sdf.write('{} {}\n'.format(*vocab_item))
    return dict_file


def fairseq_preprocess(train, val, test, fairseq_folder, hierarchy, upsample_train=True, global_dict=False):
    """

    :param train: train_df
    :param val: val_df pr validation percentage, should be a fraction from 0.0 to 1.0
    :param test: test_df
    :return:
    """

    if type(val) == float:
        assert val < 0.3, "Validation percentage cannot be larger than 30% "
        X_train, X_val = train_test_split(train.index, shuffle=True, test_size=val)
        val = train.loc[X_val]
        train = train.loc[X_train]

    if upsample_train:
        top_misclassified_categories = ['Sachbuch', 'Kinderbuch~&~Jugendbuch', 'Ratgeber']
        train, no_samples_added = over_sample_by_category(train, hierarchy, top_misclassified_categories, pct=0.15,boosting_rounds=1)
    train['input_text'].to_csv(os.path.join(fairseq_folder, 'train.des'), index=False, header=False, sep='#',
                               quoting=csv.QUOTE_NONE)
    train['labels'].to_csv(os.path.join(fairseq_folder, 'train.cat'), index=False, header=False, sep='#',
                           quoting=csv.QUOTE_NONE)

    val['input_text'].to_csv(os.path.join(fairseq_folder, 'val.des'), index=False, header=False, sep='#',
                             quoting=csv.QUOTE_NONE)
    val['labels'].to_csv(os.path.join(fairseq_folder, 'val.cat'), index=False, header=False, sep='#',
                         quoting=csv.QUOTE_NONE)

    test['input_text'].to_csv(os.path.join(fairseq_folder, 'test.des'), index=False, header=False, sep='#',
                              quoting=csv.QUOTE_NONE)
    if not 'labels' in test.columns:
       test['labels'] = '?'
    test['labels'].to_csv(os.path.join(fairseq_folder, 'test.cat'), index=False, header=False, sep='#',
                          quoting=csv.QUOTE_NONE)

    train_pref = "{}/train".format(fairseq_folder)
    val_pref = "{}/val".format(fairseq_folder)
    test_pref = "{}/test".format(fairseq_folder)
    dest_dir = os.path.join(fairseq_folder, "data")

    command = ['fairseq-preprocess',
               '--source-lang',
               'des',
               '--target-lang',
               'cat',
               '--trainpref',
               train_pref,
               '--validpref',
               val_pref,
               '--testpref',
               test_pref,
               '--destdir',
               dest_dir
               ]

    if global_dict:
        logger.debug("using a global dict for source and target")
        # create source dict
        source_dict_file = create_dict(train, val, test, fairseq_folder, 'des')
        # create target dict
        target_dict_file = create_dict(train, val, test, fairseq_folder, 'cat')
        dict_args = ["--srcdict",
                     source_dict_file,
                     "--tgtdict",
                     target_dict_file, ]
        command += dict_args

    cmd = subprocess.list2cmdline(command)
    logger.debug(cmd)
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, bufsize=1,
                               universal_newlines=True)
    for line in process.stdout:
        logger.info(line.strip())

    FASTTEXT_FOLDER = os.path.join(fairseq_folder, 'fastText')
    os.makedirs(FASTTEXT_FOLDER, exist_ok=True)
    learn_embeddings(train, val, test, FASTTEXT_FOLDER, source_dict_file, target_dict_file)
    logger.info("learnt fastText embeddingds")


def fairseq_train(fairseq_folder):
    embeddings_path = os.path.join(fairseq_folder, 'fastText')
    model_folder = os.path.join(fairseq_folder, 'model')
    os.makedirs(model_folder, exist_ok=True)
    command = ["fairseq-train",
               os.path.join(fairseq_folder, 'data'),
               '--lr',
               '0.25',
               '--clip-norm',
               '0.1',
               '--dropout',
               '0.2',
               '--max-epoch',
               '20',
               '--max-tokens',
               '36000',
               '--no-epoch-checkpoints',
               '--encoder-embed-path',
               os.path.join(embeddings_path, 'desc_embeddings.txt'),
               '--decoder-embed-path',
               os.path.join(embeddings_path, 'cat_embeddings.txt'),
               '--source-lang',
               'des',
               '--target-lang',
               'cat',
               '--save-dir',
               model_folder,
               '--encoder-layers',
               '[(100,3)] * 2 + [(200,3)] * 2 + [(300,3)]',
               '--decoder-layers',
               '[(100,3)] * 2 + [(200,3)]',
               '--encoder-embed-dim',
               '100',
               '--decoder-embed-dim',
               '100',
               '--arch',
               'fconv'
               ]
    cmd = subprocess.list2cmdline(command)
    logger.debug(cmd)
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, bufsize=1,
                               universal_newlines=True)
    for line in process.stdout:
        logger.info(line.strip())


def fairseq_predict(fairseq_folder):
    predictions_folder = os.path.join(fairseq_folder, 'results')
    os.makedirs(predictions_folder, exist_ok=True)
    command = ["fairseq-generate",
               os.path.join(fairseq_folder, 'data'),
               '--path',
               os.path.join(fairseq_folder, 'model/checkpoint_best.pt'),
               '--batch-size',
               '64',
               '--nbest',
               '1',
               '--beam',
               '5',
               '>',
               '{}/predicted.txt'.format(predictions_folder)
               ]

    cmd = subprocess.list2cmdline(command)
    logger.debug(cmd)
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, bufsize=1,
                               universal_newlines=True)
    for line in process.stdout:
        logger.info(line.strip())
