from logging import FileHandler

from germeval.evaluate import parse_predictions,evaluate_model
from germeval.fairseq import fairseq_preprocess, fairseq_train, fairseq_predict
from germeval.hierarchy import Hierarchy
from germeval.preprocessing import preprocess
import sys
import os
import string
import random
import logging
import yaml
import logging.config


def random_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))


def configure_logging(filename):
    with open('logging.conf') as log_conf:
        log_conf_dict = yaml.safe_load(log_conf)

    logging.config.dictConfig(log_conf_dict)
    germeval_logger = logging.getLogger('germeval')
    handler = FileHandler(filename)
    handler.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
    germeval_logger.addHandler(handler)


def main(data_folder):

    RUN_ID = random_generator(5)

    DATA_FOLDER = os.path.abspath(data_folder)
    PHASE3_DATA_FOLDER = os.path.join(DATA_FOLDER, 'blurbs_test_participants')
    PHASE2_DATA_FOLDER = os.path.join(DATA_FOLDER, 'blurbs_dev_participants')
    CSV_EXPORT_FOLDER = os.path.join(DATA_FOLDER, 'csv')
    os.makedirs(CSV_EXPORT_FOLDER, exist_ok=True)

    FAIRSEQ_FOLDER = os.path.join(DATA_FOLDER, 'fairseq',RUN_ID)
    os.makedirs(FAIRSEQ_FOLDER, exist_ok=True)


    configure_logging(os.path.join(FAIRSEQ_FOLDER,'run.log'))
    logger = logging.getLogger("germeval.{}".format(__name__))
    logger.info("RUN_ID:{}".format(RUN_ID))

    hierarchy = Hierarchy(os.path.join(PHASE3_DATA_FOLDER, 'hierarchy.txt'))
    logger.info("created hierarchy")

    phase3_train_books_df = preprocess(os.path.join(PHASE3_DATA_FOLDER, 'blurbs_train_all.txt'))
    phase3_test_books_df = preprocess(os.path.join(PHASE3_DATA_FOLDER, 'blurbs_test_participants.txt'))
    phase2_train_books_df = preprocess(os.path.join(PHASE2_DATA_FOLDER, 'blurbs_train.txt'))
    phase2_val_books_df = preprocess(os.path.join(PHASE2_DATA_FOLDER, 'blurbs_dev_participants.txt'))
    phase2_val_books_df['categories'] = phase3_train_books_df.loc[phase2_val_books_df.index]['categories']
    phase2_val_books_df['labels'] = phase3_train_books_df.loc[phase2_val_books_df.index]['labels']
    logger.info("xml to pandas_df transformation completed.")

    #export csv
    phase3_train_books_df.to_csv(os.path.join(CSV_EXPORT_FOLDER,'phase3_train.csv'))
    phase3_test_books_df.to_csv(os.path.join(CSV_EXPORT_FOLDER,'phase3_test.csv'))
    phase2_train_books_df.to_csv(os.path.join(CSV_EXPORT_FOLDER,'phase2_train.csv'))
    phase2_val_books_df.to_csv(os.path.join(CSV_EXPORT_FOLDER,'phase2_val.csv'))
    logger.info("CSV export completed.")

    fairseq_preprocess(phase2_train_books_df,0.05,phase2_val_books_df,FAIRSEQ_FOLDER,hierarchy,global_dict=True)
    logger.info("fairseq-preprocessing completed.")

    logger.info("staring fairseq-train.")
    fairseq_train(FAIRSEQ_FOLDER)
    logger.info("fairseq-train completed.")

    logger.info("staring fairseq-generate.")
    fairseq_predict(FAIRSEQ_FOLDER)
    logger.info("fairseq-generate completed.")

    results_df = parse_predictions(FAIRSEQ_FOLDER,hierarchy,phase2_val_books_df)
    evaluate_model(FAIRSEQ_FOLDER,results_df)
    logger.info("evaluation completed.")



if __name__ == '__main__':
    data_folder = sys.argv[1]
    os.environ['CUDA_VISIBLE_DEVICES'] = sys.argv[2]
    main(data_folder)
